%define _prefix /gem_base/epics/support
%define name iocStats
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Package, a module for EPICS base
Name: %{name}
Version: 3.1.16.0
Release: 6%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: epics-base-devel sequencer-devel python38
Requires: epics-base sequencer python38
## Switch dependency checking off
# AutoReqProv: no

%description
This is the module %{name}.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name} epics-base-devel sequencer-devel python38
Provides: /usr/bin/python
%description devel
This is the module %{name}.


%prep
%setup -q 

%build
make distclean uninstall
make

%install
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r configure $RPM_BUILD_ROOT/%{_prefix}/%{name}
find $RPM_BUILD_ROOT/%{_prefix}/%{name}/configure -name ".git" -exec rm -rf {} \;

%post devel
/usr/sbin/alternatives --set python /usr/bin/python3

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{name}
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{name}/bin
   /%{_prefix}/%{name}/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{name}/dbd
   /%{_prefix}/%{name}/db
   /%{_prefix}/%{name}/include
   /%{_prefix}/%{name}/configure

%changelog
* Tue Feb 02 2021 Tiffany Shreves <tshreves@gemini.edu> 3.1.16.0-6
- Timezone fix added substitution for RTEMS and removed unneccessary VxWorks
  substitution variable

* Tue Feb 02 2021 Tiffany Shreves <tshreves@gemini.edu> 3.1.16.0-5
- 

* Tue Feb 02 2021 Tiffany Shreves <tshreves@gemini.edu> 3.1.16.0-4
- Add epics_tz to substitutions file as part of vendor fix for illegal inp
  warning on 3.15 ioc boot

* Tue Feb 02 2021 Tiffany Shreves <tshreves@gemini.edu>
- Add epics_tz to substitutions file as part of vendor fix for illegal inp
  warning on 3.15 ioc boot

* Thu Oct 08 2020 fkraemer <fkraemer@gemini.edu> 3.1.16.0-2
- applied new version/release scheme and new yum repository structure

* Thu Aug 13 2020 Felix Kraemer
- deleted some garbage from conflict resolution
  (fkraemer@localhost.localdomain)

* Thu Aug 13 2020 Felix Kraemer
- Release tag enriched with hour and minute (%%H%%M) to be able to build
  several RPMs a day without messing up the repo (fkraemer@gemini.edu)
- Merged vendor sources from their git repo
* Wed Jul 29 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200729cb43078
- new package built with tito

* Wed Jul 29 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200729518b953
- adjusted specfile (fkraemer@gemini.edu)
- added pythn38 dependency (fkraemer@gemini.edu)

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.2020072284397ca
- finally the right Release tag (fkraemer@gemini.edu)

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722.git1ffd58e
- 

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722.gite3de128
- 

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722.gite0d0621
- 

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722.git0d132b2
- modified Release tag (fkraemer@gemini.edu)

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722d7d3ad7}
- modified Release tag (fkraemer@gemini.edu)

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu>
- 

* Wed Jul 22 2020 fkraemer <fkraemer@gemini.edu> 3.15.8-3.1.16.0.20200722
- new package built with tito

