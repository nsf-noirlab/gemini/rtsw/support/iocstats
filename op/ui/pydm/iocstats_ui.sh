#!/bin/bash

# Run iocStats ui screens
# ./iocStats_ui.sh [channelname] [epics_ca_addr_list]

# Unset pythonpath to avoid pydm access_callback error
unset PYTHONPATH

# Pass channel name (without :) as argument, default is cr1
if [ -z "$1" ]
  then
    TOP_ARG='{"ioc":"cr1:iocStats"}'
  else
    TOP_ARG='{"ioc":"'$1':iocStats"}'
fi

# Pass IP for the EPICS_CA_ADDR_LIST, default is for HBF sim4 (10.1.2.174)
if [ -z "$2" ]
  then
    export EPICS_CA_ADDR_LIST=10.1.2.174
  else
    export EPICS_CA_ADDR_LIST=$2
fi

# Launch initial iocstats rtems screen
pydm -m $TOP_ARG --hide-nav-bar --hide-menu-bar --hide-status-bar ioc_stats_rtems.ui &
